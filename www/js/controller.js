app.controller('HomeCtrl', function($scope, $cordovaGeolocation, $state) { 

    $scope.localizar = function() {
        var posOptions = {timeout: 10000, enableHighAccuracy: false};
        $cordovaGeolocation
            .getCurrentPosition(posOptions)
            .then(function (position) {
                var lat  = position.coords.latitude
                var long = position.coords.longitude

                $state.go('mapa', {'lat': lat, 'long': long});

            }, function(err) {
                console.log(err);
            });
    }

});

app.controller('MapaCtrl', function($scope, $stateParams) { 

    var latitude = $stateParams.lat;
    var longitude = $stateParams.long;

    var coordenadas = {
        lat: parseFloat(latitude),
        lng: parseFloat(longitude)
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: coordenadas
    });

    var marker = new google.maps.Marker({
        position: coordenadas,
        map: map
    });
});

app.controller('QrcodeCtrl', function($scope, $cordovaBarcodeScanner, $cordovaInAppBrowser, $ionicPlatform) {

    $scope.readCode = function() {
        $ionicPlatform.ready(function() {
            $cordovaBarcodeScanner.scan()
                .then(function(barcodeData) {

                    var defaultOptions = {
                        location: 'no',
                        clearcache: 'yes',
                        toolbar: 'yes'
                    };

                    // alert(barcodeData.text);
                    // $window.open(barcodeData.text, '_blank');
                    $cordovaInAppBrowser.open(barcodeData.text, '_system', defaultOptions);
                }, function(error) {
                    alert(error);
                });
        });
    }

});