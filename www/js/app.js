var app = angular.module('starter', ['ionic', 'ngCordova']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

app.config(function($stateProvider, $urlRouterProvider) { 
  
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'templates/home.html',
      controller: 'HomeCtrl'
    })

    .state('mapa', {
      url: '/mapa?:lat&:long',
      templateUrl: 'templates/mapa.html',
      controller: 'MapaCtrl'
    })

    .state('qrcode', {
      url: '/qrcode',
      templateUrl: 'templates/qrcode.html',
      controller: 'QrcodeCtrl'
    })
    
    $urlRouterProvider.otherwise('/home');
});