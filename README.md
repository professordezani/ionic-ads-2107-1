# Projetos IONIC/Hardware ADS #

Prof. Dr. Henrique Dezani

dezani@fatecriopreto.edu.br

## Código Ionic View ##
aa2f53df

## Projetos ##

### Geolocalização (18/05/2017) ###
- Bruno Kolenyak
- Heitor Madeira
- William Felipe
- Lúcio Flavio de C. Matias

[Gerar Key do Google Maps](https://developers.google.com/maps/documentation/javascript/)

Caso não haja permissão do navegador, utilizar o comando CLI para executar o projeto `ionic serve -l --address localhost`

### QRCode/InAppBrownser (18/05/2017) ###
- Jonatas Sgamato
- Luiz Felipe Marten Cordeiro
- Leonardo Maria Soares
- Pedro Arthur Georgetti